function openSearch() {
    $('.header__search-btn').on('click', function () {
        $('.search').addClass('opened');
        $('.search__overlay').fadeIn();
    })
}

function closeSerach() {
    $('.search__close').on('click', function () {
        $('.search').removeClass('opened');
        $('.search__overlay').fadeOut();
    });
    $('.search__overlay').on('click', function () {
        $('.search').removeClass('opened');
        $('.search__overlay').fadeOut();
    })
}

var open = false;

function openMobmenu() {
    $('.header__burger').on('click', function () {
        var mobmenu = $('.mobmenu');
        $(this).toggleClass('opened');
        mobmenu.toggleClass('opened');
        $('.mobmenu-overlay').fadeIn();
        if (open) {
            $('.mobmenu-overlay').fadeOut();
            open = false;
        } else {
            open = true;
        }
    });
    $('.mobmenu-overlay').on('click', function () {
        $('.mobmenu-overlay').fadeOut();
        $('.header__burger').removeClass('opened');
        $('.mobmenu').removeClass('opened');
        open = false;
    })
}

function instagramSliderItin() {
    var slider = $('.instagram__slider'),
        arrows = $('.js-instagram-arrows');
    slider.slick({
        infinite: true,
        slidesToShow: 7,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 880,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 415,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
    arrows.on('click', function (e) {
        e.preventDefault();
        slider.slick($(this).attr('data-slider'));
    })
}

function mainBannerInit() {
    $('.main__banner').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true
    })
}

function mainSliderInit() {
    $('.main__slider').slick({
        slidesToShow: 5,
        infinite: true,
        centerMode: true,
        centerPadding: "0px"
    })
}

function setArrowsPosition() {
    var arrows = $('.main__slider .slick-arrow'),
        imgHeight = $('.slick-center .main__slide-img img').innerHeight();
    arrows.css('min-height', imgHeight);
}

function openCeoText() {
    $('.ceo-text__btn').on('click', function () {
        $('.ceo-text').toggleClass('opened');
    })
}

function filterSort() {
    $('.filter__selected').on('click', function () {
        $('.filter__select').toggleClass('opened');
    });
    $('.filter__option a').on('click', function (e) {
        e.preventDefault();
        var text = $(this).html();
        $('.filter__selected').html(text);
        $('.filter__select').removeClass('opened');
    })
}

function itemSliderInit() {
    $('.item__slider').slick({
        infinite: true,
        arrows: true,
        dots: true
    })
}

function setCatalogView() {

    var grid = $('.js-grid-view'),
        solo = $('.js-solo-view'),
        items = $('.catalog__items');

    grid.on('click', function () {
        $(this).addClass('selected');
        solo.removeClass('selected');
        items.removeClass('solo').addClass('grid');
        $('.item__slider').slick('refresh');
    });
    solo.on('click', function () {
        $(this).addClass('selected');
        grid.removeClass('selected');
        items.removeClass('grid').addClass('solo');
        $('.item__slider').slick('refresh');
    })
}

function filterRange() {
    if ($('*').is('.catalog__filter-range-line')) {
        $(".catalog__filter-range-line").slider({
            min: filterMin,
            max: filterMax,
            values: [valueMin, valueMax],
            range: true,
            animate: "fast",
            slide : function(event, ui) {
                $(".catalog__filter-range-left").val(ui.values[ 0 ]);
                $(".catalog__filter-range-right").val(ui.values[ 1 ]);
            }
        });
        $(".catalog__filter-range-left").val($(".catalog__filter-range-line").slider("values", 0));
        $(".catalog__filter-range-right").val($(".catalog__filter-range-line").slider("values", 1));
        $(document).focusout(function() {
            var input_left = $(".catalog__filter-range-left").val().replace(/[^0-9]/g, ''),
                opt_left = $(".catalog__filter-range-line").slider("option", "min"),
                where_right = $(".catalog__filter-range-line").slider("values", 1),
                input_right = $(".catalog__filter-range-right").val().replace(/[^0-9]/g, ''),
                opt_right = $(".catalog__filter-range-line").slider("option", "max"),
                where_left = $(".catalog__filter-range-line").slider("values", 0);
            if (input_left > where_right) {
                input_left = where_right;
            }
            if (input_left < opt_left) {
                input_left = opt_left;
            }
            if (input_left == "") {
                input_left = 0;
            }
            if (input_right < where_left) {
                input_right = where_left;
            }
            if (input_right > opt_right) {
                input_right = opt_right;
            }
            if (input_right == "") {
                input_right = 0;
            }
            $(".catalog__filter-range-left").val(input_left);
            $(".catalog__filter-range-right").val(input_right);
            $(".catalog__filter-range-line").slider( "values", [ input_left, input_right ] );
        });
    }
}

function filterCollaspe() {
    $('.filter__title--collapse').on('click', function () {
        $(this).parents('.filter__collapse').toggleClass('collapsed');
    })
}

function openFilter() {
    $('.filter__btn').on('click', function () {
        $('.filter__wrap').addClass('opened');
    });
    $('.filter__close').on('click', function () {
        $('.filter__wrap').removeClass('opened');
    })
}
function cardSelect() {

    $('.card__select-arrow').on('click', function () {
        $(this).parents('.card__select').toggleClass('opened');

    });
    $('.card__option').on('click', function (e) {
        e.preventDefault();
        $('.card__option').removeClass('selected');
        $(this).addClass('selected');
        $('.card__select').removeClass('opened');

    });
    $('*').on('click', function (e) {
        var el = document.querySelector('.card__select-arrow');
        if(e.target !== el) {
            $('.card__select').removeClass('opened');
        }
    })

}
function cardSliderInit() {
    var slider = $('.card__slider'),
        arrows = $('.js-card-arrows');
    slider.slick({
        slidesToShow: 1,
        focusOnSelect: true,
        asNavFor: '.card__navslider',
        fade: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    dots: true
                }
            }
        ]
    });
    arrows.on('click', function (e) {
        e.preventDefault();
        slider.slick($(this).attr('data-slider'));
    });
    $('.card__navslider').slick({
        slidesToShow: 4,
        focusOnSelect: true,
        asNavFor: '.card__slider',
        vertical: true,
        arrows: false
    })
}

function cardTabs() {
    $('.card__tab').on('click', function() {
        $('.card__tab').removeClass('active').eq($(this).index()).addClass('active');
        $('.card__content').hide().eq($(this).index()).fadeIn()
    }).eq(0).addClass('active');
}

function alsoLikeSliderInit() {
    var slider = $('.also-like__slider'),
        arrows = $('.js-also-arrows');
    slider.slick({
        infinite: true,
        slidesToShow: 4,
        arrows: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }
        ]
    });
    arrows.on('click', function (e) {
        e.preventDefault();
        slider.slick($(this).attr('data-slider'));
    })
}

function setAlsoArrowsPosition() {
    var arrows = $('.js-arrows'),
        imgHeight = $('.also-like__img').innerHeight();
    arrows.css('min-height', imgHeight);
}

$(document).ready(function () {
    openSearch();
    closeSerach();
    openMobmenu();
    instagramSliderItin();
    mainBannerInit();
    mainSliderInit();
    setArrowsPosition();
    openCeoText();
    filterSort();
    itemSliderInit();
    setCatalogView();
    filterRange();
    filterCollaspe();
    openFilter();
    cardSelect();
    cardSliderInit();
    cardTabs();
    alsoLikeSliderInit();
    setAlsoArrowsPosition();


    $('#popup-request').validate();
    jQuery.extend(jQuery.validator.messages, {
        required: "Поле обязательно для заполнения",
        remote: "Please fix this field.",
        email: "Неправильный формат",
        url: "Please enter a valid URL.",
        date: "Неправильный формат",
        dateISO: "Please enter a valid date (ISO).",
        number: "Неправильный формат",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });

    $(window).resize(function () {
        setTimeout(function () {
            setArrowsPosition();
            setAlsoArrowsPosition();
        });
    });
});